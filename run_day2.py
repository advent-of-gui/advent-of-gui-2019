from library import intcode_computer as computer
from library import processor as pc


target = 19690720
for value1 in range(100):
    for value2 in range(100):
        memory = [1,0,0,3,1,1,2,3,1,3,4,3,1,5,0,3,2,10,1,19,2,9,19,23,2,13,23,27,1,6,27,31,2,6,31,35,2,13,35,39,1,39,10,43,2,43,13,47,1,9,47,51,1,51,13,55,1,55,13,59,2,59,13,63,1,63,6,67,2,6,67,71,1,5,71,75,2,6,75,79,1,5,79,83,2,83,6,87,1,5,87,91,1,6,91,95,2,95,6,99,1,5,99,103,1,6,103,107,1,107,2,111,1,111,5,0,99,2,14,0,0]
        memory = pc.set_noun(memory, value1)
        memory = pc.set_verb(memory, value2)
        new_memory = computer.run_intcode(memory)
        if new_memory[0] == target:
            solution = new_memory[0]
            solution_noun = value1
            solution_verb = value2
            break

print(f"To get output 19690720, you must input:")
print(f"Noun: {solution_noun}")
print(f"Verb: {solution_verb}")

final_solution = 100 * solution_noun + solution_verb
print(f"Final solution is: {final_solution}")
