from run_day1 import calculate_fuel, calculate_fuel_for_fuel


def test_calculate_fuel_mass_12():
    expected = 2
    fuel = calculate_fuel(12)
    assert fuel == expected


def test_calculate_fuel_mass_14():
    expected = 2
    fuel = calculate_fuel(14)
    assert fuel == expected


def test_calculate_fuel_mass_1969():
    expected = 654
    fuel = calculate_fuel(1969)
    assert fuel == expected


def test_calculate_fuel_mass_100756():
    expected = 33583
    fuel = calculate_fuel(100756)
    assert fuel == expected


def test_calculate_fuel_mass_1969_as_string():
    expected = 654
    fuel = calculate_fuel('1969')
    assert fuel == expected


def test_calculate_fuel_for_fuel_with_input_2():
    expected = 0
    fuel_for_fuel = calculate_fuel_for_fuel(2)
    assert fuel_for_fuel == expected


def test_calculate_fuel_for_fuel_with_input_654():
    expected = 312
    fuel_for_fuel = calculate_fuel_for_fuel(654)
    assert fuel_for_fuel == expected


def test_calculate_fuel_for_fuel_with_input_33583():
    expected = 16763
    fuel_for_fuel = calculate_fuel_for_fuel(33583)
    assert fuel_for_fuel == expected