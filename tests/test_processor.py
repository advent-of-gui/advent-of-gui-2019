import pytest

from library import processor as pc

@pytest.fixture(scope='function')
def memory():
    yield [0,1,2,3,4,5,6]


def test_get_instruction_from_0(memory):
    expected = [0,1,2,3]
    instruction = pc.get_instruction(memory, 0)
    assert instruction == expected


def test_get_instruction_from_2(memory):
    expected = [2,3,4,5]
    instruction = pc.get_instruction(memory, 2)
    assert instruction == expected


def test_get_opcode():
    instruction = [0,1,2,3]
    opcode = pc.get_opcode(instruction)
    assert opcode == 0


def test_get_values(memory):
    instruction = [0,1,2,3]
    expected = 1,2
    value1,value2 = pc.get_values(memory, instruction)
    assert value1,value2 == expected


def test_replace_value(memory):
    new_memory = pc.replace_value(memory, 5, 99)
    expected = [0,1,2,3,4,99,6]
    assert new_memory == expected


def test_run_opcode_1_two_positive_values():
    total = pc.run_opcode_1(2,7)
    assert total == 9


def test_run_opcode_1_two_negative_values():
    total = pc.run_opcode_1(-3,-4)
    assert total == -7


def test_run_opcode_1_one_positive_one_negative_value():
    total = pc.run_opcode_1(-3,8)
    assert total == 5


def test_run_opcode_2_two_positive_values():
    total = pc.run_opcode_2(2,7)
    assert total == 14


def test_run_opcode_2_two_negative_values():
    total = pc.run_opcode_2(-3,-4)
    assert total == 12


def test_run_opcode_2_one_positive_one_negative_value():
    total = pc.run_opcode_2(-3,8)
    assert total == -24


def test_set_noun(memory):
    new_memory = pc.set_noun(memory,99)
    expected = [0,99,2,3,4,5,6]
    assert new_memory == expected


def test_set_verb(memory):
    new_memory = pc.set_verb(memory,99)
    expected = [0,1,99,3,4,5,6]
    assert new_memory == expected
