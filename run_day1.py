import math

def calculate_fuel(mass) -> int:
    # mass/3, round down, -2
    mass = int(mass)
    third_of_mass = math.floor(mass/3)
    return (third_of_mass - 2)


def calculate_fuel_for_fuel(module_fuel: int) -> int:
    fuel_for_fuel = calculate_fuel(module_fuel)
    smallest_fuel = fuel_for_fuel
    while smallest_fuel > 0:
        new_fuel = calculate_fuel(smallest_fuel)
        if new_fuel < 0:
            break
        fuel_for_fuel += new_fuel
        smallest_fuel = new_fuel
    if fuel_for_fuel <= 0:
        return 0
    else:
        return fuel_for_fuel


with open("inputs\\day1_input.txt") as file:
    total_fuel = 0

    modules = file.readlines()

    for mass in modules:
        module_fuel = calculate_fuel(mass)
        fuel_for_fuel = calculate_fuel_for_fuel(module_fuel)
        total_fuel += module_fuel + fuel_for_fuel
    
print(f"The total amount of fuel needed is: {total_fuel}")
