"""Functions used by intcode computer"""

def get_instruction(memory: list, address: int) -> list:
    return memory[address:(address+4)]

def get_opcode(instruction: list) -> int:
    return instruction[0]

def get_values(memory: list, instruction: list) -> tuple:
    param1 = instruction[1]
    param2 = instruction[2]
    value1 = memory[param1]
    value2 = memory[param2]
    return value1,value2

def replace_value(memory: list, address: int, new_value: int) -> list:
    memory[address] = new_value
    return memory

def run_opcode_1(num1: int, num2: int):
    return num1 + num2

def run_opcode_2(num1: int, num2: int):
    return num1 * num2

def set_noun(memory: list, value: int) -> list:
    memory[1] = value
    return memory

def set_verb(memory: list, value: int) -> list:
    memory[2] = value
    return memory