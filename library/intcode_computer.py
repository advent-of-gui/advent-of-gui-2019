from . import processor as pc

def run_intcode(memory: list) -> list:
    address = 0
    new_memory = memory

    while (address + 4) < len(memory):
        instruction = pc.get_instruction(memory, address)
        opcode = pc.get_opcode(instruction)

        value1,value2 = pc.get_values(memory, instruction)

        if opcode == 99:
            address += 1
            break
        if opcode == 1:
           total = pc.run_opcode_1(value1, value2)
        if opcode == 2:
            total = pc.run_opcode_2(value1, value2)
        
        new_memory = pc.replace_value(memory, instruction[3], total)
        address += 4

    return new_memory

